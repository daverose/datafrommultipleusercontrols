﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<string, int> DataInMainWindow = new Dictionary<string, int>();
        private double moreData;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ChangeText_Click(object sender, RoutedEventArgs e)
        {
            //Grab the data from the user controls
            if (!DataInMainWindow.ContainsKey(myControl1.TextInUserControl))
            {
                DataInMainWindow.Add(myControl1.TextInUserControl, myControl1.TextInUserControl.Count());
            }
            if (!DataInMainWindow.ContainsKey(myControl2.TextInUserControl))
            {
                DataInMainWindow.Add(myControl2.TextInUserControl, myControl2.TextInUserControl.Count());
            }

            if (myControl1.SomeOtherData.Any())
            {
                moreData = myControl1.SomeOtherData.First();// Get some other data
            }
        }
    }
}
