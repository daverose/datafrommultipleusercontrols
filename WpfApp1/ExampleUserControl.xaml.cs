﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MyUserControl1.xaml
    /// </summary>
    public partial class ExampleUserControl : UserControl
    {
        private Random random;
        public List<double> SomeOtherData = new List<double>();

        private static readonly DependencyProperty _textInUserControl =
            DependencyProperty.Register("SomeText", typeof(string),
                typeof(ExampleUserControl), new PropertyMetadata("Some lovely text"));

        private void ChangeUserControlText_Click(object sender, RoutedEventArgs e)
        {
            SomeOtherData.Add(random.NextDouble());
        }

        public string TextInUserControl
        {
            get { return (string)GetValue(_textInUserControl); }
            set { SetValue(_textInUserControl, value); }
        }

        public ExampleUserControl()
        {
            InitializeComponent();
            random = new Random();
        }
    }
}
